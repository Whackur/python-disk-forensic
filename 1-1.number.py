# 숫자의 표현형식
print(42)
print(0o52)
print(0x2a)
print(0b101010)


# 8진수, 16진수, 2진수로 출력
print(oct(0b101010))
print(hex(0b101010))
print(bin(0b101010))


# 숫자의 형변환
oct = oct(0b101010)
print(oct)
print(int(oct, 8))
print(int('42', 10))


# format으로 진수 변환하기
print('with prefix : ', bin(42))
print('binary : ', format(42, 'b'))
print('octal : ', format(42, 'o'))
print('hex : ', format(42, 'x'))
print('hex : ', format(42, 'X'))
print('decimal : ', format(42, 'd'))


# format의 접두사 제외하기
print('with prefix : ', bin(42))
print('binary : ', format(42, '#b'))
print('octal : ', format(42, '#o'))
print('hex : ', format(42, '#x'))
print('hex : ', format(42, '#X'))
print('decimal : ', format(42, '#d'))


# print문에서의 출력형식 지정
print("int: {0:d}, bin: {0:b}, oct: {0:o}, hex: {0:x}".format(42))