# psutil 의 활용 https://psutil.readthedocs.io/en/latest/#disks
import psutil
partitions = psutil.disk_partitions()
for partition in partitions:
    print(partition)
print(psutil.disk_usage('c:\\'))


# psutil로 컴퓨터 부팅시각 반환
import psutil, datetime
print(datetime.datetime.fromtimestamp(psutil.boot_time()).strftime("%Y-%m-%d %H:%M:%S"))


# # 디스크정보 보기
import shutil, os
print(shutil.disk_usage(os.getcwd()))
x = shutil.disk_usage('c:\\')
print('Total bytes : ' + str(x.total) + 'bytes')
print('Total : ' + str(format(x.total / 1024 / 1024 / 1024, '.2f')) + 'gb')
print('Used : ' + str(format(x.used / 1024 / 1024 / 1024, '.2f')) + 'gb')
print('Free : ' + str(format(x.free / 1024 / 1024 / 1024, '.2f')) + 'gb')
print(os.getcwd())