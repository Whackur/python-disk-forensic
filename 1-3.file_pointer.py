# seek, tell, read
handle = open('./files/images/MBR.jpg', 'rb')
handle.seek(0)
print('first tell : ', handle.tell())
print(handle.read(16))
print(handle.read(16))
print('second tell : ', handle.tell())
handle.seek(0) ### 처음으로 되돌아감
print(handle.read(16))
print('third tell : ', handle.tell())


# handle MBR
handle = open('./files/images/MBR.jpg', 'rb')
handle.seek(0)
print('first tell : ', handle.tell())
print(handle.read(16))
print(handle.read(16))
print('second tell : ', handle.tell())
handle.seek(0) ### 처음으로 되돌아감
print(handle.read(16))
print('third tell : ', handle.tell())


# read binary
handle = open('./files/samples/normal.txt', 'rb')
print(handle.read(2))
print(handle.read(2))
print(handle.read(2))
handle.seek(0)
print(handle.read(2))


# seek in binary
print('### seek, tell ###')
handle = open('./files/samples/normal.txt', 'rb')
print('두번째 인자값은 절대적인 위치 0, 상대적인 위치는 1, 파일 끝은 2')
print('두번째 인자값은 절대적인 위치 os.SEEK_SET, 상대적인 위치는 os.SEEK_CUR, 파일 끝은 os.SEEK_END')
handle.seek(3, 0)
print(handle.read(2))
handle.seek(-2, 1)
print(handle.read(4))
handle.seek(-3, 2)
print(handle.read(2))
import os
handle.seek(-2, os.SEEK_END)
print(handle.read(2))
handle.seek(-3, os.SEEK_CUR)
print(handle.read(2))