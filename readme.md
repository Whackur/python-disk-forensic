# 개요
- 파이썬을 이용하여 증거수집과 같은 행위를 자동화합니다.
- 파이썬을 이용하여 디스크 구조를 이해하고 다루는 법을 배웁니다. 

# 누구에게 필요한가?

- 모의해킹에 대해 관심이 있는 사람
- 포렌식에 대해 관심이 있는 사람
- 파이썬에 대해 관심이 있는 사람
- 운영체제에 대해 관심이 있는 사람
- 디지털 포렌식 도구 제작에 관심이 있는사람
- 디지털 포렌식 관련업무를 하는 사람

# 사전 지식 & 필요 도구
- **파이썬 기초**가 있으면 더욱 좋습니다.
- **윈도우 운영체제**를 기준으로 하며 Mac, Linux의 경우 가상머신을 권장합니다.
- git
- vscode or pycharm등의 IDE. 강의는 vscode로 진행됩니다.
- FTK Imager, HxD 등...

# 공부방법
- 유튜브 강의를 참고하면서 따라오시면 됩니다.
- 아래 학습 설명서의 링크를 누르면 이동합니다.
- 파이썬 코딩이 필요가 없거나 생략하고 들으시고 싶다면 **[파이썬]** 말머리가 붙은 것은
보지 마세요.

# 주의 사항
- 디스크 쓰기(덮어씌우기) 작업은 실제 사용중인 컴퓨터를 망가뜨릴 수도 있습니다.
- 학습에 대한 내용을 충분히 숙지한 후 실습하세요.

# 개발환경 세팅
- Anaconda를 추천합니다. win32api의 이용과 python 가상환경설정에 유리합니다.
- VSCode를 사용합니다. VSCode는 관리자 권한으로 실행할 수 있도록 합니다.

# 참고 자료
git 명령어로 아래 레포지토리를 받으세요.
```
git clone https://github.com/proneer/Slides
git clone https://github.com/whackur/python-disk-forensic
```

# 학습 설명서 바로가기 
### [1-1.기본개념](docs/1-1.기본개념.md)
포렌식을 배우기에 앞서 필요한 지식들을 배웁니다.

###  
    

![MBR](/files/images/MBR.jpg)
*MBR이미지*

# 1-2