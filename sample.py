import sys
import struct


def PartitionTable(p, order):
    print("%d 번째 파티션"%(order+1))

    # Boot Indicator
    if p[0] == 0x00:
        print("1. Boot Indicator : 0x00 = 부팅 불가")
    elif p[0] == 0x80:
        print("1. Boot Indicator : 0x80 = 부팅 가능")
    else:
        print("1. Boot Indicator : 비정상 구글링 ㄱㄱ")

    # Start Sector
    start = struct.unpack('<L', p[8:8+4])[0]
    print("2. Start Sector : %d"%start)
    
    # 파티션 타입 확인
    PartitionType = {0x00:'Empty', 0x01:'FAT12 Primary', 0x04:'FAT (up to 32MB)', 0x05:'MS Extended (확장 파티션)',
                     0x06:'BIGDOS FAT16 (33MB ~ 4GB)', 0x07:'NTFS, IFS(OSX Installable FilsSystem)', 0x0B:'FAT32', 0x0C:'FAT32',
                     0x82:'Solaris x86', 0x83:'Linux', 0xA5:'FreeBSD', 0xEE:'EFI GPT Disk'}

    print("3. Partition Type : 0x%x "%(p[4]) + PartitionType[p[4]])

    # Sector Size
    length = struct.unpack('<L', p[12:12+4])[0]
    print("4. Total Sector : %d" % length)

    print("-----------------------------------------")
    print("")




def main():
    if len(sys.argv) is 1:
        print("파일명을 입력하세요")
        return

    try:
        f = open(sys.argv[1], 'rb')

        # 디스크의 맨 앞으로 이동
        f.seek(0)

        # MBR의 크기만큼 바이트를 읽어 버퍼에 담음
        mbr = f.read(512)

        # MBR의 Magic Number를 이용하여 제대로 읽혀졌는지 체크
        if mbr[510] == 0x55 and mbr[511] == 0xAA:
            print("MBR Read Success")
        else:
            print("MBR Read Fail")
            print("프로그램을 종료합니다.")
            return

        print("-----------------------------------------")

        part = []

        # 파티션 정보 획득
        for i in range(4):
            part.append(mbr[446 + (i * 16): 446 + (i * 16) + 16])

        # 4번째 필드를 확인하여 각 파티션 타입을 확인
        for i in range(4):
            p = part[i]

            if p[4] != 0:
                PartitionTable(p, i)

        f.close()

    except Exception as e:
        print(e)


main()
