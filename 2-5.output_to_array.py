import os, subprocess
# Memory Information
# WMIC ComputerSystem Get TotalPhysicalMemory
# WMIC ComputerSystem Get TotalPhysicalMemory /Format:Value
# WMIC ComputerSystem Get TotalPhysicalMemory | findstr /v TotalPhysicalMemory


powershell_path = subprocess.check_output(["where", "powershell"]).decode().replace('\\','/').replace('\r\n','')
output = subprocess.check_output([powershell_path, "WMIC ComputerSystem Get TotalPhysicalMemory | findstr /v TotalPhysicalMemory"]).decode().strip()
memory_size = int(output) / 1024 / 1024 /1024
print('리소스 모니터의 물리메모리 크기와 비교하기')
print('물리 메모리 크기 : ', round(memory_size, 2))


# CPU Temperature (kelvin)
powershell_path = subprocess.check_output(["where", "powershell"]).decode().replace('\\','/').replace('\r\n','')
output = subprocess.check_output([powershell_path, "wmic /namespace:\\\\root\wmi PATH MSAcpi_ThermalZoneTemperature get CurrentTemperature | findstr /v CurrentTemperature"]).decode().strip()
kelvin = int(output.splitlines()[0].strip()) / 10
celsius = kelvin - 273.15
print('CPU 온도 : ', round(celsius, 2))
