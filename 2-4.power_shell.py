import os

# https://docs.microsoft.com/en-us/powershell/module/storage/get-disk?view=win10-ps
os.system('powershell.exe Get-Disk')
# https://docs.microsoft.com/en-us/powershell/module/storage/get-physicaldisk?view=win10-ps
os.system('powershell.exe Get-PhysicalDisk')
os.system('powershell.exe Get-Disk -SerialNumber 12172218E0027')
os.system('powershell.exe Get-Disk -SerialNumber 12172218E0027')
# Working on cmd...
os.system('powershell.exe Get-CimInstance -ClassName Win32_OperatingSystem | select *')
os.system('powershell.exe Get-CimInstance -ClassName Win32_OperatingSystem | select LastBootUpTime')


# subprocess + Popen
import subprocess
cmd = subprocess.Popen('C:/Windows/System32/WindowsPowerShell/v1.0/powershell.exe Get-CimInstance -ClassName Win32_OperatingSystem | select *', stdout=subprocess.PIPE)
cmd_out, cmd_err = cmd.communicate()
print(cmd_out)
print(cmd_err)


# subprocess + check_output
import subprocess
output = subprocess.check_output(["C:/Windows/System32/WindowsPowerShell/v1.0/powershell.exe", "Get-CimInstance -ClassName Win32_OperatingSystem | select *"])
print(output)
output = subprocess.check_output(["C:/Windows/System32/WindowsPowerShell/v1.0/powershell.exe", "Get-CimInstance -ClassName Win32_BIOS"])
print(output) # binary string need to be decode...


# powershell path
import os
import subprocess
powershell_path = subprocess.check_output(["where", "powershell"]).decode()
print(powershell_path)
powershell_path = powershell_path.replace('\\','/').replace('\r\n','')
print(powershell_path)


# finally
import os
import subprocess
powershell_path = subprocess.check_output(["where", "powershell"]).decode().replace('\\','/').replace('\r\n','')
output = subprocess.check_output([powershell_path, "Get-CimInstance -ClassName Win32_OperatingSystem | select LastBootUpTime"]).decode('euc-kr')
print(output)