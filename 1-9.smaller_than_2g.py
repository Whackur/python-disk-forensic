# Q. Physical Drive의 경로를 넣었을 때 크기가 4gb 보다 크면 False
# 작으면 True를 반환하는 함수를 작성해봅시다.
# 추후 테스트용 드라이브인지 검증할 때 사용할 함수입니다.

import shutil
def bigger_than_4g(path):
    x = shutil.disk_usage(path)
    total_size = float(format(x.total / 1024 / 1024 / 1024, '.2f'))
    return total_size < 4

if __name__ == "__main__":
    print(bigger_than_4g('\\\\.\\PhysicalDrive2'))