import os

# wmi를 사용한 쿼리 실행
os.system('wmic diskdrive list brief')
os.system('wmic diskdrive get name,model,serialnumber,size,status')


# Logical Drive Volume Size
os.system('wmic LogicalDisk Where DriveType=3 Get DeviceID, VolumeName, Size, FreeSpace /Format:Value')


# Memory Information
os.system('WMIC ComputerSystem Get TotalPhysicalMemory /Format:Value')
os.system('WMIC OS Get FreePhysicalMemory /Format:Value')


# Current CPU Information
os.system('WMIC CPU Get Name, LoadPercentage /Format:Value')


# CPU Temperature (kelvin)
os.system('wmic /namespace:\\\\root\wmi PATH MSAcpi_ThermalZoneTemperature get CriticalTripPoint,CurrentTemperature')